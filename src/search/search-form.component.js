angular
  .module("search") //
  .component("searchForm", {
    bindings: {
      query: "<value",
      onSearch: "&"
    },
    // <div ng-form name="$ctrl.forms[5].subform"></div>
    // <pre>{{ $ctrl.queryForm.queryField | json }}</pre>
    template: `<form name="$ctrl.queryForm"> 
    <style>
      .input-group .ng-touched.ng-invalid,
      .input-group .ng-dirty.ng-invalid{ border:2px solid red; }
    </style>
    <div class="input-group">
        <input class="form-control" type="text"
          name="queryField"
          minlength="3"
          required
          ng-model="$ctrl.query" 
          ng-on-keyup="$event.key == 'Enter' && 
        $ctrl.search()">

        <div class="input-group-append">
          <input type="button" value="Search" 
                class="btn btn-outline-secondary" 
                ng-click="$ctrl.search()">
        </div>
    </div>
    <div ng-if="$ctrl.queryForm.queryField.$touched || $ctrl.queryForm.queryField.$dirty">
      <p ng-if="$ctrl.queryForm.queryField.$error.required">Query cannot be empty</p>
      <p ng-if="$ctrl.queryForm.queryField.$error.minlength">Query has to have 3 letters</p>
    </div>
  </form>`,

    controller: function() {
      this.queryForm;

      this.search = function() {
        this.onSearch({
          $event: this.query
        }); // send event to parent

        this.query = "";
      };
    }
    // , controllerAs:'$ctrl' // directive
  });
