var search = angular.module("search", []);
require("../services/music-search.service");
require("./search-form.component");

search.controller("SearchCtrl", function(
  MusicSearch,
  $scope,
  $state,
  $stateParams
) {
  this.$onInit = function() {
    // const query = $state.params["query"];
    const query = $stateParams["query"];
    if (query) {
      this.search(query);
    } else {
      this.redirect(MusicSearch.state.query);
    }
  };

  this.message = "";
  this.state = MusicSearch.state;

  this.redirect = function(query) {
    // $state.transitionTo('search',{query},{ location: true, inherit: true, relative: router.globals.$current, notify: true })

    // $state.go(".", { query });

    $state.go(".", { query }, { location: "replace" });
  };

  this.search = function(query) {
    if (query.length < 3) {
      return;
    }
    if (query === this.state.query) {
      return;
    }
    if (query.includes("batman")) {
      return;
    }
    $scope.$emit("notification", "Searching for " + query);

    MusicSearch.search(query) //
      .then(
        function(albums) {
          this.query = query;
          this.results = albums;
        }.bind(this)
      )
      .catch(
        function(err) {
          this.message = err.message;
        }.bind(this)
      );
  };

  // this.search("batman");
});

require("./search-results.css");

search.component("searchResults", {
  bindings: { results: "<" },
  template: `
  <div class="card-group">
    <album-card class="card"
      album="result"
      ng-repeat="result in $ctrl.results track by result.id">
    </album-card>
  </div>`
});

search.component("albumCard", {
  bindings: { album: "<" },
  template: ` 
  <img class="card-img-top" 
    ng-src="{{$ctrl.album.images[0].url}}" >
  <div class="card-body">
    <h5 class="card-title">
      {{$ctrl.album.name}}
    </h5>
  </div>`
});

// $scope.results = [
//   {
//     id: 1,
//     name: "Album 1",
//     images: [
//       {
//         url: "https://www.placecage.com/gif/200/300"
//       }
//     ]
//   }
// ];
