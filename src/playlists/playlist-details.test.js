require("./playlists.module");
require("./playlist-details");

// https://docs.angularjs.org/api/ngMock

describe("playlist-details component", () => {
  var $compile, $rootScope;

  // Load the myApp module, which contains the directive
  beforeEach(module("playlists"));

  // Store references to $rootScope and $compile
  // so they are available to all tests in this describe block
  beforeEach(inject(function(_$compile_, _$rootScope_) {
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  it("should render playlist", () => {
    var element = $compile(`<playlist-details playlist="playlist" on-edit="funkcja($event)">
    </playlist-details>`)($rootScope);

    $rootScope.playlist = {
      id: 12,
      name: "TEst",
      favorite: true,
      color: "#ff00ff"
    };
    $rootScope.funkcja = jasmine.createSpy("funkcja");

    $rootScope.$digest();

    expect(element[0].querySelector(".playlist-name").textContent).toMatch(
      "TEst"
    );
    expect(element[0].querySelector(".playlist-favorite").textContent).toMatch(
      "Yes"
    );
    expect(element[0].querySelector(".playlist-color").style.color).toEqual(
      "rgb(255, 0, 255)"
    );

    element[0]
      .querySelector("input[type=button]")
      .dispatchEvent(new Event("click"));
    expect($rootScope.funkcja).toHaveBeenCalled();
  });

  // https://docs.angularjs.org/guide/unit-testing#testing-a-controller
  // it('sets the strength to "strong" if the password length is >8 chars', function() {
  //   var $scope = $rootScope.$new();
  //   var controller = $controller('PasswordController', { $scope: $scope });
  //   $scope.password = 'longerthaneightchars';
  //   $scope.grade();
  //   expect($scope.strength).toEqual('strong');
  // });

  // https://docs.angularjs.org/api/ngMock/service/$componentController
  // it("should test component", () => {
  //   inject($componentController => {
  //     var compontnt = $componentController(
  //       componentName,
  //       { $scope: {}, service: {} },
  //       {
  //         playlist: {},
  //         onEdit: jasmine.createSpy()
  //       },
  //       // '$ctrl'
  //     );
  //   });
  // });


  // https://docs.angularjs.org/api/ngMock/service/$httpBackend

  // https://docs.angularjs.org/api/ngMock/service/$timeout
  // flush() -> finish all pending Timeout / Interval
  // flush(200) -> finish all pending Timeout / Interval under 200ms
  
  // https://docs.angularjs.org/api/ngMock/function/browserTrigger  
});
