var playlistsModule = angular.module("playlists", []);

require('./playlist-details')
require('./playlist-form')
require('./playlists-list')

playlistsModule.constant("playlists_data", [
  {
    id: 123,
    name: "Angular Hits",
    favorite: true,
    color: "#ff00ff"
  },
  {
    id: 234,
    name: "Angular TOP20 ",
    favorite: true,
    color: "#ff00ff"
  },
  {
    id: 345,
    name: "Best of Angular",
    favorite: true,
    color: "#ff00ff"
  }
]);

playlistsModule.service("PlaylistsService", function(playlists_data) {
  this._playlists = playlists_data;

  this.getPlaylists = function() {
    return this._playlists;
  };

  this.save = function(draft) {
    if (draft.id) {
      var index = this._playlists.findIndex(p => p.id === draft.id);
      if (index !== -1) {
        this._playlists.splice(index, 1, draft);
      }
    } else {
      draft.id = new Date().getTime();
      this._playlists.push(draft);
    }
  };
});

playlistsModule.controller("PlaylistsCtrl", function($scope, PlaylistsService) {
  this.playlists = PlaylistsService.getPlaylists();

  this.save = function(draft) {
    draft = Object.assign({}, this.selected, draft);

    PlaylistsService.save(draft);

    this.mode = "show";
    this.selected = draft;
  };

  this.mode = "show";

  this.edit = function(event) {
    this.mode = "edit";
  };

  this.cancel = function() {
    this.mode = "show";
  };

  this.showCreateNewForm = function() {
    this.mode = "edit";
    this.selected = {};
  };

  this.selected = null;

  this.select = function(p) {
    if (this.selected && this.selected.id == p.id) {
      this.selected = null;
    } else {
      this.selected = p;
    }
  };
});
