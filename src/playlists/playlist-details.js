// playlist-details.tpl.html

var playlistsModule = angular.module("playlists");
var detailsTpl = require('./playlist-details.tpl.html')

playlistsModule.directive("playlistDetails", function() {

  return {
    scope:{
      // @ = < &
      playlist:'<', 
      onEdit:'&'
    },
    template: detailsTpl,
    // templateUrl:'src/playlists/playlist-details.tpl.html',
    controller:function($scope){

    }
  }

});
