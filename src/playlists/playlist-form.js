var playlists = angular.module("playlists");

/**
 *
 * @param {angular.IScope} scope
 * @param {angular.IAugmentedJQuery} element
 * @param {angular.IAttributes} attrs
 * @param {angular.IController} ctrl
 */

playlists.directive("usernameTaken", function($http, $q) {
  return {
    scope: { 
      usernameTaken: "<", 
      usernameTakenExtra: "<", 
      name: "@"
    },
    restrict: "A",
    require: ["ngModel", "^?form"],
    link: function(scope, $element, attrs, required, ctrl) {
      var [ngModel, form] = required;
      // ngModel.$validators["usernameTaken"] = function(model, view) {
      //   return !("" + view).includes("superman");
      // };

      ngModel.$asyncValidators["usernameTaken"] = function(model, view) {
        // return $http.get(...).then(function(res){ return .. }

        return $q(function(resolve) {
          setTimeout(function() {
            if (("" + view).includes("superman")) {
              resolve($q.reject("cooklwiek"));
            } else {
              resolve(true);
            }
          }, 3000);
        });
      };
    }
  };
});

playlists.component("playlistForm", {
  template: require("./playlist-form.tpl.html"),

  bindings: {
    playlist: "<",
    onCancel: "&",
    onSave: "&"
  },

  controller: class PlaylistForm {
    $onChanges() {
      this.draft = { ...this.playlist };
    }

    $postLink() {
      this.playlistForm["name"].$validators["censor"] = function(
        modelValue,
        viewValue
      ) {
        console.log(viewValue);
        return !("" + viewValue).includes("batman");
      };
    }

    save() {
      if (this.playlistForm.$invalid) {
        return;
      }

      this.onSave({
        $event: this.draft
      });
    }
  }
});
