var playlists = angular.module("playlists");

playlists.component("playlistsList", {
  templateUrl: "src/playlists/playlists-list.tpl.html",
  bindings: {
    playlists:'<',
    selected:'<',
    onSelect:"&"
  },
  controller: class PlaylistsList{
    select(playlist){
      this.onSelect({$event:playlist})
    }
  }
})