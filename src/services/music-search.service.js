angular
  .module("search") //
  .service("MusicSearch", MusicSearch);

/**
 * @param {angular.IHttpService} $http
 */
function MusicSearch($http, Auth, $rootScope) {
  this.state = {
    query: "",
    results: []
  };

  this.search = function(query = "batman") {
    this.state.query = query;

    return $http
      .get("https://api.spotify.com/v1/search", {
        params: {
          type: "album",
          query: query
        }
      })
      .then(function(resp) {
        return resp.data.albums.items;
      })
      .then(
        function(results) {
          this.state.results = results;

          return results;
        }.bind(this)
      );
  };
}

// .catch(err => {
//   if (err.status == 401) {
//     Auth.authorize();
//   }
//   console.log(err.data.error.message);
// });
// class MusicSearch{}
