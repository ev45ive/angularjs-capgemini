import "./playlists/playlists.module";
import "./search/search.module";
import "./auth/auth.module";
// import 'angular-ui-router'
// https://christopherthielen.github.io/ui-router-extras/#/home
import "@uirouter/angularjs";
import "bootstrap/dist/css/bootstrap.css";

var app = angular.module(
  "app", //
  ["playlists", "search", "auth", "ui.router"]
);

app.config(function(AuthProvider, $httpProvider) {
  AuthProvider.setConfig({
    authUrl: "https://accounts.spotify.com/authorize",
    params: {
      client_id: "70599ee5812a4a16abd861625a38f5a6",
      response_type: "token",
      redirect_uri: "http://localhost:8080/"
      // show_dialog:true
    }
  });
  /*
    https://developer.spotify.com/dashboard/ 
    placki@placki.com
    ******
  */
});

app.run(function(Auth) {
  Auth.initialize();
});

/* === */

app.controller("AppCtrl", function($scope, $timeout) {
  $scope.user = { name: "Bob" };
  $scope.notifications = [];

  $scope.$on("notification", function(event, data) {
    $scope.notifications.push({ message: data });
    $timeout(function() {
      $scope.notifications.shift();
    }, 3000);
  });
});

// https://github.com/angular-ui/ui-router/wiki/URL-Routing
app.config(function(
  // Ui.Router
  $stateProvider,
  $urlRouterProvider,
  $locationProvider
) {
  // webpack historyApiFallback
  // $locationProvider.html5Mode(true);
  $urlRouterProvider.when("", "/");
  $urlRouterProvider.otherwise("/");

  $stateProvider
    // #!/
    .state({
      name: "root",
      url: "/",
      template: `<div>Hello</div>`
    })
    .state({
      name: "about",
      url: "/about",
      template: `<div>about</div>`
    })
    .state({
      url: "/playlists",
      name: "playlists",
      templateUrl: "views/playlists.tpl.html"
    })
    .state({
      url: "/search?query&type",
      name: "search",
      templateUrl: "views/search.tpl.html",
      controller: "SearchCtrl",
      controllerAs: "$ctrl"
    });

  /* ==== */

  $stateProvider
    .state({
      name: "layout",
      url: "/layout",
      // abstract: true,
      redirectTo: "layout.index",
      template: `
        <div class="row">
          <div class="col">Lewa   <ui-view></ui-view></div>
          <div class="col">Prawa  <ui-view name="details"></ui-view></div>
        </div>
      `,
      controller($scope) {
        $scope.placki = 123;
      }
    })
    .state({
      name: "layout.index",
      url: "/",
      template: `<h1>Index {{placki}}</h1> <a ui-sref="menu">Menu</a> <ui-view></ui-view>`
    })
    .state({
      parent: "layout",
      name: "menu",
      url: "/menu",
      views: {
        $default: {
          template: `<h1>Default?</h1>  <a ui-sref="^">Index</a>`
        },
        details: {
          template: "<h1>menu</h1> <ui-view></ui-view>"
        }
      }
    });
});

angular.bootstrap(document, ["app"]);
