angular
  .module("auth", [])
  // Auth Service Provider
  .provider("Auth", function() {
    var _config = {};

    this.setConfig = function(config) {
      _config = config;
    };

    this.$get = [
      "$location",
      "$window",
      function($location, $window) {
        return new Auth(_config, $location, $window);
      }
    ];
  })
  // ============
  // https://docs.angularjs.org/api/ng/service/$http#interceptors
  .service("authHttpInterceptor", function(Auth, $q) {
    this.request = function(config) {
      config.headers["Authorization"] = "Bearer " + Auth.getToken();
      return config;
    };

    this.responseError = function(rejection) {
      if (rejection.status === 401) {
        Auth.authorize();
      }
      // https://docs.angularjs.org/api/ng/service/$q
      return $q.reject(new Error(rejection.data.error.message));
    };
  })
  .config(function($httpProvider) {
    $httpProvider.interceptors.push("authHttpInterceptor");
  })

// ==================================

function Auth(_config, $location, $window) {
  this._token = "";

  this.initialize = function() {
    this._token = JSON.parse($window.sessionStorage.getItem("token"));

    if (!this._token) {
      var params = new URLSearchParams($location.hash());
      this._token = params.get("access_token");
      $location.hash("");
    }

    if (!this._token) {
      this.authorize();
    } else {
      $window.sessionStorage.setItem("token", JSON.stringify(this._token));
    }
  };

  this.authorize = function() {
    $window.sessionStorage.removeItem("token");
    var params = new URLSearchParams(_config.params);
    var url = _config.authUrl + "?" + params;
    $window.location.replace(url);
  };

  this.getToken = function() {
    return this._token;
  };
}

// .config(function(AuthProvider) {
//   AuthProvider.setConfig({
//     // Detault options
//   })
// });

// debugger;
// return $q(function(resolve, reject) {
//   setTimeout(function() {
//     resolve({
//       data: {
//         albums: {
//           items: [
//             {
//               name: "Test error"
//             }
//           ]
//         }
//       }
//     });
//   }, 3000);
// });
