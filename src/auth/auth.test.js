require("./auth.module");

// Wepack.DefinePlugin:
// var module = window.module;
// var inject = window.inject;

describe("Auth module", () => {
  var $window, Auth;

  beforeEach(module("auth"));

  beforeEach(() => {
    $window = {
      location: { replace: "" },
      sessionStorage: jasmine.createSpyObj("sessionStorage", [
        "getItem",
        "setItem",
        "removeItem"
      ])
    };

    module($provide => {
      $provide.value("$window", $window);
    });

    inject(_Auth_ => (Auth = _Auth_));
  });

  it("should create auth service", () => {
    var spy = spyOn($window.location, "replace");

    $window.sessionStorage.getItem.and.returnValue('""');

    Auth.initialize();
    
    expect($window.sessionStorage.getItem).toHaveBeenCalledWith("token");
    expect(spy).toHaveBeenCalled();
  });
});
