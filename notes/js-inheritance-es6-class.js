function Person(name) {
  this.name = name;
}
Person.prototype.sayHello = function() {
  return "Hi I am " + this.name;
};

function Employee(name, salary) {
  Person.apply(this, [name]);
  this.salary = salary;
}
Employee.prototype = Object.create(Person.prototype);

Employee.prototype.work = function() {
  return "I want my " + this.salary;
};

class Person {
  constructor(name) {
    this.name = name;
  }
  sayHello() {
    return "Hi, I am " + this.name;
  }
}

class Employee extends Person {
  constructor(name, salary) {
    super(name);
    this.salary = salary;
  }
  work() {
    return "I want my " + this.salary;
  }
}
tom = new Employee("Tom", 4000);
