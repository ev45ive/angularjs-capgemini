// https://www.npmjs.com/package/karma-webpack

// require all modules ending in ".test.js" from the
// current directory and all subdirectories


const testsContext = require.context("./src/", true, /\.test\.js$/);

testsContext.keys().forEach(testsContext);

// npm i --global karma-cli

// npm i --save-dev karma  karma-jasmine karma-chrome-launcher jasmine-core

// npm i --save-dev karma-webpack karma-sourcemap-loader
