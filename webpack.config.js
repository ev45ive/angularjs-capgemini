const path = require("path");
const webpack = require("webpack");

/*
 * SplitChunksPlugin is enabled by default and replaced
 * deprecated CommonsChunkPlugin. It automatically identifies modules which
 * should be splitted of chunk by heuristics using module duplication count and
 * module category (i. e. node_modules). And splits the chunks…
 *
 * It is safe to remove "splitChunks" from the generated configuration
 * and was added as an educational example.
 *
 * https://webpack.js.org/plugins/split-chunks-plugin/
 *
 */

const HtmlWebpackPlugin = require("html-webpack-plugin");

/*
 * We've enabled HtmlWebpackPlugin for you! This generates a html
 * page for you when you compile webpack, which will make you start
 * developing and prototyping faster.
 *
 * https://github.com/jantimon/html-webpack-plugin
 *
 */

//  https://github.com/philipwalton/webpack-esnext-boilerplate
//  https://philipwalton.com/articles/deploying-es2015-code-in-production-today/

// https://webpack.js.org/plugins/define-plugin/#root

// NpmInstallWebpackPlugin
// Auto-install missing dependencies during development
// https://github.com/webpack-contrib/awesome-webpack#webpack-plugins

module.exports = {
  mode: "development",

  entry: {
    main: "./src/main.js"
  },

  output: {
    filename: "[name].[chunkhash].js",
    path: path.resolve(__dirname, "dist")
  },

  // devtool: "sourcemap",
  devtool: "inline-source-map",

  plugins: [
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "src", "index.html")
    }),
    // new ExtractTextWebpackPlugin("styles.css")
  ],

  module: {
    rules: [
      {
        test: /.(js|jsx)$/,
        include: [path.resolve(__dirname, "src")],
        loader: "babel-loader",

        options: {
          plugins: [
            "syntax-dynamic-import",
            "@babel/plugin-proposal-class-properties"
          ],

          presets: [
            [
              "@babel/preset-env",
              {
                modules: false
              }
            ]
          ]
        }
      },
      {
        test: /.(css)$/,
        loaders: ["style-loader", "css-loader"]
      },
      {
        test: /\.(tpl\.html)$/,
        use: {
          loader: "html-loader",
          options: {
            attrs: [":data-src"]
          }
        }
      }
    ]
  },

  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          priority: -10,
          test: /[\\/]node_modules[\\/]/
        }
      },

      chunks: "async",
      minChunks: 1,
      minSize: 30000,
      name: true
    }
  },

  // https://webpack.js.org/configuration/dev-server/
  devServer: {
    open: true,
    historyApiFallback: true // bez hasha!
  }
};
